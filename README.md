Python package for VGG Datasets
===============================

The vgg-datasets Python package is the one stop package to get and use
VGG datasets through Python.
