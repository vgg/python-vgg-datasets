#!/usr/bin/env python3

## Copyright 2023 David Miguel Susano Pinto <pinto@robots.ox.ac.uk>
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     https://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.

import logging
import os
import os.path
from typing import Any, List

from vgg_datasets import MD5Checksum, PlainArchive, TarArchive
from vgg_datasets.torch import VGGTorchDataset, _open_image_file


_logger = logging.getLogger(__name__)


_N_IMGS_PER_VERSION = {
    "1.0": 1440191,
    "2.0": 1439719,
    "3.0": 1439588,
}


class PASS(VGGTorchDataset):
    """`PASS: Pictures without humAns for Self-Supervision <https://www.robots.ox.ac.uk/~vgg/data/pass/>`__

    PASS is a large-scale image dataset that does not include any
    humans and which can be used for high-quality pretraining while
    significantly reducing privacy concerns.

    Args:

        root: Root directory for the dataset.  This is where the
            `PASS_dataset` directory and `pass_metadata.csv` file can
            be found.

    Versions:

       v1: initial release: 1,440,191 images

        v2: Removed 472 images, now 1,439,719 images. Thanks to the
          Know-your-data
          (https://knowyourdata-tfds.withgoogle.com/#dataset=pass)
          page (published on the 13.10.2021), we were able to identify
          472 further images that contained humans.  Most images that
          we have removed only contained human depictions (e.g. in
          newspapers, black-white portraits, ads) in some background,
          and very few were actual photographs of people (<50). We
          used KYD to sort images both by face area and face
          probablity to find all images that were missed in v1.  We
          have further added more metadata that will aid further
          analysis in KYD in the future.

        v3: Compared to v2.0 we have removed further 131 images that
            mostly contained faces, other body parts or images of
            tattoos.

    """

    _ARCHIVES = {
        "1.0": [
            PlainArchive(
                archive_relpath="pass_metadata.csv",
                dataset_relpath="pass_metadata.csv",
                url="https://zenodo.org/record/5528345/files/pass_metadata.csv",
                checksum=MD5Checksum("b2d8938da2ddfcaecf93d1af81f0ec0a"),
            ),
            TarArchive(
                archive_relpath="PASS.0.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/5528345/files/PASS.0.tar",
                checksum=MD5Checksum("a19eb0894e457d4e9c2b5517bbde8d7d"),
            ),
            TarArchive(
                archive_relpath="PASS.1.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/5528345/files/PASS.1.tar",
                checksum=MD5Checksum("c5769de320f4cfb5623a1bed55995091"),
            ),
            TarArchive(
                archive_relpath="PASS.2.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/5528345/files/PASS.2.tar",
                checksum=MD5Checksum("70b54214c7f2cc57f6069a275f375354"),
            ),
            TarArchive(
                archive_relpath="PASS.3.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/5528345/files/PASS.3.tar",
                checksum=MD5Checksum("b647812c558da3ab45f5cab4f866c91f"),
            ),
            TarArchive(
                archive_relpath="PASS.4.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/5528345/files/PASS.4.tar",
                checksum=MD5Checksum("451d1355a3fc8a3a742ed83d10380508"),
            ),
            TarArchive(
                archive_relpath="PASS.5.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/5528345/files/PASS.5.tar",
                checksum=MD5Checksum("af6a7cf2644650ca56a6669b22c4f3ee"),
            ),
            TarArchive(
                archive_relpath="PASS.6.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/5528345/files/PASS.6.tar",
                checksum=MD5Checksum("4fcb80fa597730799bd5419ce99124de"),
            ),
            TarArchive(
                archive_relpath="PASS.7.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/5528345/files/PASS.7.tar",
                checksum=MD5Checksum("51ee83dac260ca11e8059ff33b3fdad1"),
            ),
            TarArchive(
                archive_relpath="PASS.8.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/5528345/files/PASS.8.tar",
                checksum=MD5Checksum("882b7f10c14a67d3fe47624b2340ea49"),
            ),
            TarArchive(
                archive_relpath="PASS.9.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/5528345/files/PASS.9.tar",
                checksum=MD5Checksum("e978af85e64d10f55f7e2710757c2234"),
            ),
        ],
        "2.0": [
            PlainArchive(
                archive_relpath="pass_metadata.csv",
                dataset_relpath="pass_metadata.csv",
                url="https://zenodo.org/record/5570664/files/pass_metadata.csv",
                checksum=MD5Checksum("270548aa4652444c22888b23044c8648"),
            ),
            TarArchive(
                archive_relpath="PASS.0.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/5570664/files/PASS.0.tar",
                checksum=MD5Checksum("423e228e841af3a8e8a2430a704bc1e1"),
            ),
            TarArchive(
                archive_relpath="PASS.1.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/5570664/files/PASS.1.tar",
                checksum=MD5Checksum("aca7dabd8e690b1ae492241997883050"),
            ),
            TarArchive(
                archive_relpath="PASS.2.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/5570664/files/PASS.2.tar",
                checksum=MD5Checksum("1d29fc5f17fef4588108bbd7c8e9a943"),
            ),
            TarArchive(
                archive_relpath="PASS.3.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/5570664/files/PASS.3.tar",
                checksum=MD5Checksum("fd5055282950a9f041bd238e05327d45"),
            ),
            TarArchive(
                archive_relpath="PASS.4.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/5570664/files/PASS.4.tar",
                checksum=MD5Checksum("f06a531bc902dd1b8fdce0a7b875763b"),
            ),
            TarArchive(
                archive_relpath="PASS.5.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/5570664/files/PASS.5.tar",
                checksum=MD5Checksum("e21fe96811df927defcdbf8f494ede66"),
            ),
            TarArchive(
                archive_relpath="PASS.6.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/5570664/files/PASS.6.tar",
                checksum=MD5Checksum("0c9e80d5f9df693e977243d6afcebf6d"),
            ),
            TarArchive(
                archive_relpath="PASS.7.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/5570664/files/PASS.7.tar",
                checksum=MD5Checksum("9d3e8d5cd81936e80e75067ab9d0e20c"),
            ),
            TarArchive(
                archive_relpath="PASS.8.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/5570664/files/PASS.8.tar",
                checksum=MD5Checksum("1b2dac2a48b796068fea1b0c9ddc623e"),
            ),
            TarArchive(
                archive_relpath="PASS.9.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/5570664/files/PASS.9.tar",
                checksum=MD5Checksum("2aa450912db229e2621a586f70fcdfbb"),
            ),
        ],
        "3.0": [
            PlainArchive(
                archive_relpath="pass_metadata.csv",
                dataset_relpath="pass_metadata.csv",
                url="https://zenodo.org/record/6615455/files/pass_metadata.csv",
                checksum=MD5Checksum("0b033707ea49365a5ffdd14615825511"),
            ),
            TarArchive(
                archive_relpath="PASS.0.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/6615455/files/PASS.0.tar",
                checksum=MD5Checksum("0be3a104d6257d83296460b419f82c71"),
            ),
            TarArchive(
                archive_relpath="PASS.1.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/6615455/files/PASS.1.tar",
                checksum=MD5Checksum("dddaf2840997cb181572863587899b53"),
            ),
            TarArchive(
                archive_relpath="PASS.2.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/6615455/files/PASS.2.tar",
                checksum=MD5Checksum("9b9b9e1ee65747e47ad835a4f99c8e70"),
            ),
            TarArchive(
                archive_relpath="PASS.3.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/6615455/files/PASS.3.tar",
                checksum=MD5Checksum("e2c85293cbe6178477eea7d277d95dd7"),
            ),
            TarArchive(
                archive_relpath="PASS.4.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/6615455/files/PASS.4.tar",
                checksum=MD5Checksum("a2d174f7042d98ad037cda0e3313ea00"),
            ),
            TarArchive(
                archive_relpath="PASS.5.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/6615455/files/PASS.5.tar",
                checksum=MD5Checksum("2be85748c184b058f8d95a886dab161b"),
            ),
            TarArchive(
                archive_relpath="PASS.6.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/6615455/files/PASS.6.tar",
                checksum=MD5Checksum("83e07eee33df5fcc63a4b0db4e94abb4"),
            ),
            TarArchive(
                archive_relpath="PASS.7.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/6615455/files/PASS.7.tar",
                checksum=MD5Checksum("3f708993aa168b57306e5de08ba71373"),
            ),
            TarArchive(
                archive_relpath="PASS.8.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/6615455/files/PASS.8.tar",
                checksum=MD5Checksum("f4f87af4327fd1a66dd7944b9f59cbcc"),
            ),
            TarArchive(
                archive_relpath="PASS.9.tar",
                dataset_relpath="",
                url="https://zenodo.org/record/6615455/files/PASS.9.tar",
                checksum=MD5Checksum("0429e77aa029f9ff6e1a6a78efde1c33"),
            ),
        ],
    }

    def __init__(
        self, root: str, version: str, check_exists: bool = False
    ) -> None:
        super().__init__(root, version)
        if check_exists and not self._data_exists_at(self._root, version):
            raise Exception(
                "data does not exist (or is not complete) at '%s'" % self._root
            )

        self._img_fpaths: List[str] = []
        # In data_exists_at we do check that no extra files have been
        # added to the directories but maybe we shouldn't do that and
        # instead list only the stuff from the metadata file (still
        # need to then find on which subdirectory the image is).
        dataset_dir = os.path.join(self._root, "PASS_dataset")
        for walk_root, dirs, fnames in os.walk(dataset_dir):
            del dirs
            self._img_fpaths.extend(
                [os.path.join(walk_root, fname) for fname in fnames]
            )

        expected_n_imgs = _N_IMGS_PER_VERSION[self._version]
        if expected_n_imgs != len(self._img_fpaths):
            # TODO: maybe add a different message if number matches another version
            raise Exception(
                "found unexpected number of images for version %s"
                % self._version
            )

    # XXX: In Torchvision, it returns a tuple where the second value
    # is target/class/etc but in here we have nothing.  Should we
    # always return a tuple and here target is None, depend on the
    # dataset (datasets are not interchaneably anyway), or shold we
    # have two dataset classes?
    def __getitem__(self, index: int) -> Any:
        return _open_image_file(self._img_fpaths[index])

    def __len__(self) -> int:
        return len(self._img_fpaths)

    @classmethod
    def _data_exists_at(cls, root: str, version: str) -> bool:
        if not super()._data_exists_at(root, version):
            return False

        dataset_dirpath = os.path.join(root, "PASS_dataset")
        if not os.path.isdir(dataset_dirpath):
            _logger.info(
                "data does not exist because directory '%s' does not exist",
                dataset_dirpath,
            )
            return False

        # Images are split over 21 directories (named "0" to "20")
        # within "PASS_dataset".  Their filename is the hash on the
        # "pass_metadata.csv" file plus file extension.  We want to
        # check that all images in "pass_metadata.csv" are present but
        # we don't know on which subdirectory the image files will be.
        # We just list get a list of all filenames and check if it
        # exists somewhere.
        fnames_list = []
        for walk_root, dirs, files in os.walk(dataset_dirpath):
            del walk_root
            del dirs
            fnames_list.append(files)

        fnames = set(fnames_list)
        assert len(fnames_list) == len(fnames), "filenames should be unique"

        metadata_fpath = os.path.join(root, "pass_metadata.csv")
        with open(metadata_fpath, encoding="utf-8") as fh:
            fh.readline()  # discard header line
            for line in fh:
                fields = line.split(",")
                fname = fields[4] + ".jpg"
                if fname not in fnames:
                    _logger.info(
                        "dataset directory ''%s is there but is not complete"
                        " because file '%s' can't be found",
                        dataset_dirpath,
                        fname,
                    )
                    return False

        return True


assert (
    set(_N_IMGS_PER_VERSION.keys()) == PASS.get_known_versions()
), "known PASS dataset versions mismatch"
