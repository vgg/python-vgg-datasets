#!/usr/bin/env python3

## Copyright 2023 David Miguel Susano Pinto <pinto@robots.ox.ac.uk>
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     https://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.

import contextlib
import logging
import tempfile
from typing import List, Optional

import PIL.Image
import torch.utils.data

from vgg_datasets import Archive


_logger = logging.getLogger(__name__)


def _open_image_file(fpath: str) -> PIL.Image.Image:
    return PIL.Image.open(fpath).convert("RGB")


class VGGTorchDataset(torch.utils.data.Dataset):
    """Base class for all dataset classes in this package.

    Note: this subclasses from PyTorch's ``Dataset`` and not
    Torchvision's ``VisionDataset``.

    XXX: should we add support for transforms like torchvision?

    version is always required, even if a dataset has so far only one
    version..

    """

    def __init__(self, root: str, version: str) -> None:
        if self._is_unknown_version(version):
            raise Exception(
                "unknown version '%s' (known are '%s')"
                % (version, self.get_known_versions())
            )

        self._root = root
        self._version = version

    @classmethod
    def download(
        cls,
        root: str,
        *,
        version: str,
        exists_ok: bool = False,
        remove_archives: bool = True,
        archives_dir: Optional[str] = None,
    ) -> None:
        """Download this dataset.

        Check if data exists.  If data does not exist, checks if
        archive files have previously been downloaded.  If not, then
        downloads the files one at a time and extracts.  If we could
        trust people on the internet, we could unpack the archive as
        we download them but we can't so we need to download the whole
        file to check checksum and only then do we unpack it.

        Args:

            root: where to extract the files.  This is same that
                should be passed to the class constructor.  Missing
                paths are constructed.

            version: version number/name.  Might error if no longer
                available.  This ensures.

            remove_archives: whether the downloaded files should be
                downloaded after extracting, zip and tarballs.  The
                data files remain,only archives are deleted.

           archives_dir: the file will be created in that directory;
               otherwise, a default directory is used.  If
               ``remove_archives`` is ``True``, it defaults to the
               same as used by ``tempfile``, otherwise it defaults to
               the same value as ``root``.

        .. note::

            Downloading the archive to ``root`` may cause issues if at
            the root of the archive is a file with the same name as
            the archive itself.  While it is technically possible for
            such archive to exist, we don't expect for someone to be
            that evil.

        """
        if cls._is_unknown_version(version):
            raise Exception(
                "unknown version '%s' (known are '%s')"
                % (version, cls.get_known_versions())
            )

        if cls._data_exists_at(root, version):
            if exists_ok:
                return
            else:
                raise Exception("Data already exists")

        archives = cls._get_archives_for(version)

        if remove_archives:
            archive_root_context = tempfile.TemporaryDirectory(
                dir=archives_dir
            )
        else:
            if archives_dir is None:
                archive_root_context = contextlib.nullcontext(root)
            else:
                archive_root_context = contextlib.nullcontext(archives_dir)

        with archive_root_context as archives_root:
            for archive in archives:
                archive.download_and_extract(
                    archives_root, root, remove_archives
                )

    @classmethod
    def _data_exists_at(cls, root: str, version: str) -> bool:
        """Check that the data is there (the extracted file, not the archives).

        This is only a minimal fallback.  Subclasses should call this
        with ``super`` and extend it with dataset specific / holistic
        checks and by subclassing the Archive classes with specific
        implementations of ``Archive.is_certainly_not_extracted_at`.

        """
        for archive in cls._get_archives_for(version):
            if archive.is_certainly_not_extracted_at(root):
                _logger.info(
                    "data does not exist because '%s' is not extracted",
                    archive,
                )
                return False
        return True

    @classmethod
    def _get_archives_for(cls, version: str) -> List[Archive]:
        return cls._ARCHIVES[version].copy()

    @classmethod
    def _is_unknown_version(cls, version: str) -> bool:
        return version not in cls._ARCHIVES

    @classmethod
    def get_known_versions(cls) -> List[str]:
        return list(cls._ARCHIVES.keys())
