#!/usr/bin/env python3

## Copyright 2023 David Miguel Susano Pinto <pinto@robots.ox.ac.uk>
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     https://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.

import gzip
import hashlib
import logging
import os
import os.path
import shutil
import tarfile
from typing import Optional
import importlib.metadata

import requests

_logger = logging.getLogger(__file__)

_OUR_VERSION = importlib.metadata.version("vgg-datasets")

_USER_AGENT = f"python/vgg_datasets version '{OUR_VERSION}'"


def _url_download(url: str, filepath: str) -> None:
    headers = {"user-agent": _USER_AGENT}
    with requests.get(url, headers=headers, stream=True) as response:
        os.makedirs(os.path.basename(filepath), exist_ok=True)
        with open(filepath, "wb") as fh:
            for chunk in response.iter_content(chunk_size=None):
                fh.write(chunk)


class Checksum:
    _HASH_ALGORITHM: Optional[str] = None

    def __init__(self, value: str) -> None:
        self._value = value
        if self._HASH_ALGORITHM is None:
            raise NotImplementedError(
                "class attribute _HASH_ALGORITHM not set"
            )

    def check_file(self, fpath) -> bool:
        hasher = hashlib.new(self._HASH_ALGORITHM, usedforsecurity=False)
        with open(fpath, "rb") as fh:
            hasher.update(fh.read())
        _logger.debug(
            "%s hash for '%s' is (expecting '%s')",
            fpath,
            hasher.hexdigest(),
            self._value,
        )
        return hasher.hexdigest() == self._value


class MD5Checksum(Checksum):
    _HASH_ALGORITHM = "md5"


class SHA512Checksum(Checksum):
    _HASH_ALGORITHM = "sha512"


class Archive:
    """Individual dataset archive files.

    Args:
        archive_relpath: where to download the archive including the
            archive filename itself.  This is typically the basename
            of the url but may also include some parent directory if
            it is part of a large dataset with multiple files that may
            overwrite themselves.

        dataset_relpath: where to extract the archive.  Typically,
            this is an empty string to extract the archive in whatever
            will be the dataset root.  In the case of plain files, it
            is typically the filepath that the "archive" is copied to.

        url: URL from where to download the file.

        checksum: checksum to confirm file integrity.

    """

    def __init__(
        self,
        archive_relpath: str,
        dataset_relpath: str,
        url: str,
        checksum: Checksum,
    ) -> None:
        self._archive_relpath = archive_relpath
        self._dataset_relpath = dataset_relpath
        self._url = url
        self._checksum = checksum

    def download_and_extract(
        self, archive_root: str, dataset_root: str, remove_archive: bool = True
    ) -> None:
        self._download(archive_root)
        self._extract(archive_root, dataset_root, remove_archive)

    def _download(self, archive_root: str) -> None:
        if self.is_at(archive_root):
            _logger.info("not downloading because it already exists")
            return

        archive_fpath = os.path.join(archive_root, self._archive_relpath)
        _logger.info("downloading '%s' to '%s'", self._url, archive_fpath)
        _url_download(self._url, archive_fpath)
        if not self._checksum.check_file(archive_fpath):
            raise Exception()

    def _extract(
        self, archive_root: str, dataset_root: str, remove_archive: bool
    ) -> None:
        raise NotImplementedError()

    def is_at(self, root: str) -> bool:
        """Is the original archive (unextracted) at location."""
        fpath = os.path.join(root, self._archive_relpath)
        if not os.path.isfile(fpath):
            return False
        else:
            return self._checksum.check_file(fpath)

    def is_certainly_not_extracted_at(self, root: str) -> bool:
        """We are sure the extracted files are missing from location.

        The method name with ``_certainly_`` is to hint that this may
        return ``False`` even though the archive has not been
        extracted.  Since the archive may have been removed, we don't
        know the list of files to check.  For most actual archives we
        can never be sure.

        Without ``_certainly_`` the method would be
        ``is_not_extracted_at`` which sounds much more authorative
        than it can.

        """
        del root
        return False


class TarArchive(Archive):
    """tar file without compression"""

    def _extract(
        self, archive_root: str, dataset_root: str, remove_archive: bool
    ) -> None:
        archive_fpath = os.path.join(archive_root, self._archive_relpath)
        dataset_fpath = os.path.join(dataset_root, self._dataset_relpath)
        _logger.info(
            "extracting tar file '%s' to '%s'",
            archive_fpath,
            dataset_fpath,
        )
        with tarfile.open(archive_fpath, mode="r") as tar:
            tar.extractall(dataset_fpath)
        if remove_archive:
            _logger.info("after extracting it, removing archive '%s'")
            os.remove(archive_fpath)


class TarGzArchive(TarArchive):
    """tar file with gzip compression"""

    # tarfile.open (which is what TarArchive uses) automatically
    # identifies if the tarfile is compressed or not so we can rely on
    # TarArchive to do the right thing.
    pass


class PlainArchive(Archive):
    """A plain text file such as a CSV file without compression."""

    def is_certainly_not_extracted_at(self, root: str) -> bool:
        """Is the extracted files at location."""
        fpath = os.path.join(root, self._dataset_relpath)
        if not os.path.isfile(fpath):
            return True
        else:
            return not self._checksum.check_file(fpath)

    def _extract(
        self, archive_root: str, dataset_root: str, remove_archive: bool
    ) -> None:
        archive_fpath = os.path.join(archive_root, self._archive_relpath)
        dataset_fpath = os.path.join(dataset_root, self._dataset_relpath)
        if archive_fpath == dataset_fpath:
            # This happens when the archive files are downloaded to
            # the same directory as the where the dataset will be.
            _logger.info(
                "file '%s' was downloaded directly to final location",
                archive_fpath,
            )
        else:
            if remove_archive:
                _logger.info(
                    "moving file '%s' to '%s' instead of copy and delete",
                    archive_fpath,
                    dataset_fpath,
                )
                shutil.move(archive_fpath, dataset_fpath)
            else:
                _logger.info(
                    "copy file '%s' to '%s'",
                    archive_fpath,
                    dataset_fpath,
                )
                shutil.copyfile(archive_fpath, dataset_fpath)


class PlainGzArchive(Archive):
    """A plain text file with gzip compression."""

    def _extract(
        self, archive_root: str, dataset_root: str, remove_archive: bool
    ) -> None:
        archive_fpath = os.path.join(archive_root, self._archive_relpath)
        dataset_fpath = os.path.join(dataset_root, self._dataset_relpath)

        with gzip.open(archive_fpath, "rb") as archive_fh:
            with open(dataset_fpath, "wb") as dataset_fh:
                dataset_fh.write(archive_fh.read())

        if remove_archive:
            _logger.info(
                "removing file '%s' after decompression", archive_fpath
            )
            os.remove(archive_fpath)
